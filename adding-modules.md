# Ading modules

## Student side scenario

1. Add the node type and id to the NodeType enum found at the bottom of [ScenarioGraphApi.ts](react-client/src/api/v1/ScenarioGraphApi.ts)

2. Add a new node in the [graph-nodes](react-client/src/case-logic-graph-nodes) folder, extending the base class GraphNode
	a. Override the constructor, making sure to initialise the base class
      * The nodeData object passed to the constructor will contain all of the data set in with the graph node
	
    b. Write your implementation for getNext()
      * The first parameter will be a `StateInfo` object containing the current state of the scenario, this obect is reconstructed every time the method is called and modifying it **will not** modify the actual state of the scenario in any way, however modifying the objects it contains will
      * The secons parameter will be of type `any` and contains data sent from the rendered component. This parameter will be `undefined` if the node does not render a component
      * The method should return a tuple with the first element containing `string | null` and the second element containing `any`
      * The first element should be the string id of the next node, or null if this node is the final node in the graph
      * The second element will contain information being sent to the front-end component. If the scenario is progressing into a new node, it's best practice to set this as `undefined`

3. Add the node type from step 1, and class from step 2, to the nodeTypes object at the bottom of CaseGraph.ts

4. Create a functional component for your node if it needs to be rendererd
    a. The function name must begin with a capital letter, and for best practice should be named \<Node-type>Component
    
    b. The function should take in a single argument, `INodeProps`, which contains the current state of the scenario and a function for progressing the state
      * As mentioned previously, modifying the StateInfo object directly will not affect the actual state of the scenario
      * The progress function accepts `any` type, and contains the data passed to `getNext()` 
    
    c. The fucntion should return a react JSX element 
    
	d. Make sure to add the component to the node you made in **step 2**
      * The component variable should be the function itself, not any data returned by a call of the function
