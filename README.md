# Medical Learning Management System

The application is an LMS for testing clinial decision making amongst medical students. 

## Technologies Used
* [React](https://reactjs.org/) - Web Library used to build the client
* [TypeScript](https://www.typescriptlang.org/) - Language used for client and server
* [React Bootstrap](https://react-bootstrap.github.io) - React component library used from client, based on Bootstrap
* [NPM](https://www.npmjs.com/) - Dependency Management Tool
* [NestJS](https://nestjs.com) - Framework used to build the server
* [PostgreSQL](https://www.postgresql.org/) - Database Management System used by the application
* [React Flow Chart](https://github.com/MrBlenny/react-flow-chart) - Library used for the scenario editor interface
* [i18next](https://www.i18next.com/) - Library used for handling translations and language detection


### Assumptions
* You must have node with version 10.17.0 or greater setup.
* You must have npm of version 6.11.3 or greater setup.
* You must have PostgreSQL version 11.7 or greater setup.

### Demo production

A demo production deployment can be found [here.](https://example.medical-lms.lukewarlow.uk)
Superuser Username: super@example.com
Password: Password123

## Server
### Environment Variables
* DB_USER: this is the username for the database user. String.
* DB_PASSWORD: this is the password for the database user. String.
* DB_SCHEMA: this is the schema name for the database. String.
* DB_HOST: this is the hostname for the database. String
* DB_PORT: this is the port for the database (usually 5432). Number
* DB_RESET: this sets whether the database resets on server bootup, this is usually true for development, and false on production. Needs to be true on first run in production for seed to run. true or false
* NODE_ENV: this sets the node environment. development or production
* REQUIRE_HTTPS: this is neccesary for url strings such as SSO callbacks. (Usually true in production). true or false.
* BASE_DOMAIN: this is the domain for the front-end. (eg. localhost:3000). String.
* SERVER_DOMAIN: this is the domain for the server. (eg. localhost:4000). String.

BASE_DOMAIN and SERVER_DOMAIN would normally be the same in production, when using a reverse proxy such as nginx.


### Production Setup
Start from root folder of project

```
cd server
npm install
npm run build
```

If you want to run the server manually, you can run 
```
node dist/main.js
```
This requires you to have the environment variables set.

For a production setup you would normally make use of a service file. An example of this can be seen in the root folder for the project in a file named 'medical-lms.service'. These are used for handling restarting of the application server on system reboot.

An example of an nginx config can also bee seen within the root folder.


### Development Setup
Start from root folder of project
```
cd server
npm install
npm run start
```


## React Client
### Production Setup
Start from root folder of project

```
cd react-client
npm install
npm run build
```
This will generate a built version of the front end. Ready to be used by your reverse proxy.

Default production subdomain is "openuni". So go to 
http(s)://openuni.<domain>

Admin username: admin@cardiff.ac.uk
Password: Password123

### Development Setup
Start from root folder of project

```
cd react-client
npm install
npm run start
```
This will run on http://localhost:3000.

Development username: smithj@cardiff.ac.uk
Development password: Password123

## Authors
* **Luke Warlow**
* **Iestyn Rimmer**
* **Owen Lever**    
